<?php

/**
 *
 */
namespace Sohi\CurrencyConverter\Controller\Convert;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;

use Sohi\CurrencyConverter\Service\CurrencyConverterApi\Wrapper;

/**
 * Class Currency
 * @package Sohi\CurrencyConverter\Controller\Convert
 */
class Currency extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $resultJsonFactory;
    protected $curlWrapper;

    /**
     * Currency constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Wrapper $curlWrapper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Wrapper $curlWrapper
    )
    {
        $this->resultJsonFactory    = $resultJsonFactory;
        $this->curlWrapper          = $curlWrapper;

        return parent::__construct($context);
    }

    /**
     * returns JSON data with "result" property signaling success
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $result = [];

        try {
            $dataToSend = $this->curlWrapper->getCurrencyExchangeResultData();

            $result['result']   = true;
            $result['data']     = $dataToSend;
        }
        catch (\Exception $e)
        {
            $result['result']   = false;
            $result['msg']      = $e->getMessage();
        }

        $resultObject = $this->resultJsonFactory->create()->setData(
            $result
        );

        return $resultObject;
    }
}