<?php

/**
 *
 */
namespace Sohi\CurrencyConverter\Service\CurrencyConverterApi;

use Braintree\Exception;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Action\Context ;

/**
 * Class Currency
 * @package Sohi\CurrencyConverter\Controller\Convert
 */
class Wrapper
{
    protected $curl;
    protected $context;

    /**
     * link to api endpoint
     *
     * @var string
     */
    protected $apiLink = 'https://free.currencyconverterapi.com/api/v5/convert?q=RUB_PLN&compact=y';

    /**
     * Wrapper constructor.
     * @param Context $context
     * @param Curl $curl
     */
    public function __construct(
        Context $context,
        Curl $curl
    ) {
        $this->curl = $curl;
        $this->context = $context;
    }

    /**
     * returns currency exchange (from RUB to PLN) data value based on
     * request parameter "value"
     *
     * @return float
     * @throws Exception
     */
    public function getCurrencyExchangeResultData()
    {
        //get request value for exchange
        $RUBAmount = (float)$this->context->getRequest()->getParam('value');

        if ($RUBAmount <= 0.0) {
            throw new Exception('Provided value can not be converted.');
        }

        //get exchange rate from api by curl
        try {
            $this->curl->get($this->apiLink);
            $curlResponse = $this->curl->getBody();
            $curlResponseDecoded = \Zend_Json::decode($curlResponse);

            if (!isset($curlResponseDecoded['RUB_PLN']['val'])) {
                throw new Exception('Error in api response.');
            }

            $RUB2PLNExchangeRate = (float)$curlResponseDecoded['RUB_PLN']['val'];
        }
        catch (\Exception $e) {
            throw new Exception('API error: ' . $e->getMessage() );
        }


        //
        return $RUBAmount * $RUB2PLNExchangeRate;
    }
}